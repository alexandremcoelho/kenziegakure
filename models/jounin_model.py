from ninja_model import Ninja


class Jounin(Ninja):
    ninja_level = 'Jounin'
    
    def __init__(self, name, clan, village, proficiency):
        super().__init__( name, clan, village, self.ninja_level)
        self.proficiency = proficiency
        self.is_in_mission = False

    def list_best_proficiency(self):
        high = max(self.proficiency, key=self.proficiency.get)
        return f'{self.name} demonstra maior proficiência em {high}'

    def start_mission(self):
        if self.is_in_mission == False:
            self.is_in_mission = True
            return f'O ninja {self.name} {self.clan} saiu em missão'
        
        return f'O ninja {self.name} {self.clan} já está em uma missão'
    def return_from_mission(self):

        if self.is_in_mission == True:
            self.is_in_mission = False
            return f'O ninja {self.name} {self.clan} retornou em segurança da missão'
        
        return f'O ninja {self.name} {self.clan} não está em nenhuma missão no momento'
    
